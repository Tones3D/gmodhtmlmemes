var is_fc_loaded = 0;
(function() {

// Localize jQuery variable
var jqxhr;
var jQuery;
var get_joke = 'http://www.laughfactory.com/';
var s3cdn = 'http://cdn.laughfactory.com/images/';
/******** Load jQuery if not present *********/
if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.8.3') {
    var script_tag = document.createElement('script');
    script_tag.setAttribute("type","text/javascript");
    script_tag.setAttribute("src",
        "http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js");
    if (script_tag.readyState) {
      script_tag.onreadystatechange = function () { // For old versions of IE
          if (this.readyState == 'complete' || this.readyState == 'loaded') {
			  if($.ui === undefined || $.ui.version !== '1.8.18'){
				 var script_tag_ui = document.createElement('script');
				script_tag_ui.setAttribute("type","text/javascript");
				script_tag_ui.setAttribute("src",
					"https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js");
				if (script_tag_ui.readyState) {
				  script_tag_ui.onreadystatechange = function () { // For old versions of IE
					  if (this.readyState == 'complete' || this.readyState == 'loaded') {
						  //scriptLoadHandler();
						  alert($.ui.version);
					  }
				  };
				} else {
				  //script_tag.onload = scriptLoadHandler;
				}
				// Try to find the head, otherwise default to the documentElement
				(document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag_ui);
			  }
              scriptLoadHandler();
          }
      };
    } else {
      script_tag.onload = scriptLoadHandler;
    }
    // Try to find the head, otherwise default to the documentElement
    (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
} else {
    // The jQuery version on the window is the one we want to use
	
	if($.ui === undefined || $.ui.version !== '1.8.18'){
		 var script_tag = document.createElement('script');
		script_tag.setAttribute("type","text/javascript");
		script_tag.setAttribute("src",
			"https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js");
		if (script_tag.readyState) {
		  script_tag.onreadystatechange = function () { // For old versions of IE
			  if (this.readyState == 'complete' || this.readyState == 'loaded') {
				  //scriptLoadHandler();
				  alert($.ui.version);
			  }
		  };
		} else {
		  //script_tag.onload = scriptLoadHandler;
		}
		// Try to find the head, otherwise default to the documentElement
		(document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
	}
	
    jQuery = window.jQuery;
	var script_tag_fc = document.createElement('script');
		script_tag_fc.setAttribute("type","text/javascript");
		script_tag_fc.setAttribute("src",
			get_joke+"js/facescroll.js");
		if (script_tag_fc.readyState) {
		  script_tag_fc.onreadystatechange = function () { // For old versions of IE
			  if (this.readyState == 'complete' || this.readyState == 'loaded') {
				  //scriptLoadHandler();
				  //alert($.ui.version);
			  }
		  };
		} else {
		  //script_tag.onload = scriptLoadHandler;
		}
		// Try to find the head, otherwise default to the documentElement
		(document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag_fc);
    getJokeOfTheDay();
}

/******** Called once jQuery has loaded ******/
function scriptLoadHandler() {
    // Restore $ and window.jQuery to their previous values and store the
    // new jQuery in our local jQuery variable
    jQuery = window.jQuery.noConflict(true);
    // Call our main function
    getJokeOfTheDay(); 
}

/******** Our main function ********/
function getJokeOfTheDay() {
	  
    jQuery(document).ready(function($) {
		 
        /******* Load CSS *******/
        var css_link = $("<link>", { 
            rel: "stylesheet", 
            type: "text/css", 
            href: s3cdn+"widget/css/LF_joke_of_the_day_v11.css" 
        });
        css_link.appendTo('head'); 
		//
		var css_scr = $("<link>", { 
            rel: "stylesheet", 
            type: "text/css", 
            href: s3cdn+"widget/css/jquery.custom-scrollbar.css" 
        });
        css_scr.appendTo('head'); 
		$("#joke-widget-container").html('<img src="'+s3cdn+'ajax_loader.gif" width="25">');
        /******* Load HTML *******/
        var jsonp_url = get_joke+"home/getJokeoftheDay/?callback=?";
        jqxhr = $.getJSON(jsonp_url, function(data) {
			var html = '';
			if(data){
				html = '<div class="preview-box-cont"><div id="joke-widget-container"><div class="jokeoftheday-outer"><div class="jokeoftheday-header"><div class="jokeoftheday-header-bg"><div class="jokeoftheday-logo"><a href="http://staging.laughfactory.com/" target="_blank"><img src="'+data.s3cdn+'widget/logo.png" alt="" title=""></a></div><div class="jokeoftheday-logo-text">Joke Of The Day</div></div></div><div class="jokeoftheday-content"><div class="jokeoftheday-box-outer"><div class="jokeoftheday-icon"><img src="'+data.s3cdn+'widget/joke-icon.png" alt="" title=""></div><div class="jokeoftheday-box" id="joke_custom_scroll"  ><p>'+data.joke_text+'</p></div><div class="jokeoftheday-box-img"><img src="'+data.s3cdn+'widget/comment-box-arrow.png" alt="" title=""></div><div class="jokeoftheday-name"><h6>'+data.joke_submitter+'</h6></div></div></div><div class="jokeoftheday-footer"><div class="jokeoftheday-footer-email"><a href="'+data.site_url+'" target="_blank">www.laughfactory.com</a></div><div class="jokeoftheday-more-jokes-btn"><a href="'+data.site_url+'jokes" target="_blank">More Jokes</a></div></div></div></div></div>';
				
				
				
				/*html = '<div class="jokeoftheday-outer"><div class="jokeoftheday-header"><div class="jokeoftheday-header-bg"><div class="jokeoftheday-logo"><a href="'+data.site_url+'" target="_blank"><img src="'+data.s3cdn+'widget/logo.png" alt="" title="" /></a></div><div class="jokeoftheday-logo-text">Joke Of The Day</div></div></div><div class="jokeoftheday-content"><div class="jokeoftheday-box-outer"><div class="jokeoftheday-icon"><img src="'+data.s3cdn+'widget/joke-icon.png" alt="" title="" /></div><div class="jokeoftheday-box"><p>'+data.joke_text+'</p></div><div class="jokeoftheday-box-img"><img src="'+data.s3cdn+'widget/comment-box-arrow.png" alt="" title="" /></div><div class="jokeoftheday-name"><h6>'+data.joke_submitter+'</h6></div></div></div><div class="jokeoftheday-footer"><div class="jokeoftheday-footer-email"><a href="'+data.site_url+'" target="_blank">www.laughfactory.com</a></div><div class="jokeoftheday-more-jokes-btn"><a href="'+data.site_url+'jokes" target="_blank">More Jokes</a></div></div></div>';*/
				
			}
          $('#joke-widget-container').html(html);
		  //my_test_fun();
		  //jQuery('#joke_custom_scroll').alternateScroll();
        }).done(function() {});
		//jqxhr.complete(function() { console.log( "second complete" ); $('#joke_custom_scroll').alternateScroll();});
    });
}
setTimeout('updateScr()',50);
})(); // We call our anonymous function immediately
function updateScr(){
	//alert('success');
	var h = $('#joke_custom_scroll').hasClass('alternate-scroll');
	console.log(h);
	if(!h){
		setTimeout('updateScr()',5);
		if (is_fc_loaded==1) { 
		  $('#joke_custom_scroll').alternateScroll();
		}
		
		
	}else{
		
	}
}

if(! $.isFunction($.fn.curCSS)) {
   $.curCSS = $.css; 
   $.fn.curCSS = $.fn.css; 
   var mouseY, lastY = 0; 
}			
